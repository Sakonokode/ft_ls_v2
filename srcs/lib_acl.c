#include "../includes/ft_ls.h"

int btree_node_is_leave(t_btree_node const *node)
{
        return (node && node->left == NULL && node->right == NULL);
}

int btree_node_is_root(t_btree_node const *node)
{
        return (node && node->parent == NULL);
}

