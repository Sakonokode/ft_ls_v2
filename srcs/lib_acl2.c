#include "../includes/ft_ls.h"

void    getflag_init(t_get_flags *get_flags,
        char const *available_flags,
        t_get_flags_err_cb cb)
{
    const size_t    count = ft_strlen(available_flags);
    size_t  i;

    i = 0;
    get_flags->fgpairs = (t_get_flags_pair *)malloc(sizeof(*get_flags->fgpairs)
            * (count));
    while (i < count)
    {
        get_flags->fgpairs[i].char_value = available_flags[i];
        get_flags->fgpairs[i].enabled = FALSE;
        i++;
    }
    get_flags->count = count;
    get_flags->error_cb = cb;
    get_flags->available_flags = ft_strdup(available_flags);
}

void    getflag_destroy(t_get_flags *get_flags)
{
    if (get_flags->fgpairs)
    {
        free(get_flags->fgpairs);
        get_flags->fgpairs = NULL;
    }
    if (get_flags->available_flags)
    {
        free(get_flags->available_flags);
        get_flags->available_flags = NULL;
    }
    get_flags->count = 0;
}

t_bool  getflag_enable(t_get_flags *get_flags, char c)
{
    size_t  i;

    i = 0;
    while (i < get_flags->count)
    {
        if (get_flags->fgpairs[i].char_value == c)
        {
            get_flags->fgpairs[i].enabled = TRUE;
            return (TRUE);
        }
        i++;
    }
    return (FALSE);
}

t_bool  get_flags_is_enabled(t_get_flags const *get_flags, char c)
{
    size_t  i;

    i = 0;
    while (i < get_flags->count)
    {
        if   (get_flags->fgpairs[i].char_value == c)
            return (get_flags->fgpairs[i].enabled);
        i++;
    }
    return (FALSE);
}

char const  *getflag_available_flags(t_get_flags const *get_flags)
{
    return (get_flags->available_flags);
}

