#include "../includes/ft_ls.h"

void    btree_prefix(t_btree const *tree, t_apl_func apl)       
{
    if (apl && tree && tree->root)
        btree_node_prefix(tree->root, apl);
}

void    btree_infix(t_btree const *tree, t_apl_func apl)
{
    if (apl && tree && tree->root)
        btree_node_infix(tree->root, apl);
}

void    btree_postfix(t_btree const *tree, t_apl_func apl)
{
    if (apl && tree && tree->root)
        btree_node_postfix(tree->root, apl);
}
