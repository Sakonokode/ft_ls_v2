#include "../includes/ft_ls.h"

t_bool  get_flags_enable(t_get_flags *get_flags, char c)
{
    size_t  i;            

    i = 0;
    while (i < get_flags->count)
    {
        if (get_flags->fgpairs[i].char_value == c)
        {
            get_flags->fgpairs[i].enabled = TRUE;
            return (TRUE);
        }
        i++;
    }
    return (FALSE);
}

t_bool  get_flags_is_enabled(t_get_flags const *get_flags, char c)
{
    size_t  i;

    i = 0;
    while (i < get_flags->count)
    {
        if (get_flags->fgpairs[i].char_value == c)
            return (get_flags->fgpairs[i].enabled);
        i++;
    }
    return (FALSE);
}
