#include "../includes/ft_ls.h"

t_bool  filter_dot(char const *str)
{
    return ((ft_strequ  (str, "..") || ft_strequ(str, ".")) ? FALSE : TRUE);
}

t_bool  ft_get_path(char const *dir_path, t_filter_path filter    ,
        t_btree *results)
{
    DIR             *dir;
    struct dirent   *it;

    dir = NULL;
    it = NULL;
    dir = opendir(dir_path);
    while ((it = readdir(dir))) 
    {
        if (filter == NULL || filter(it->d_name))
            btree_insert(results, ft_strdup(it->d_name));
    }
    closedir(dir);
    return (TRUE);
}
