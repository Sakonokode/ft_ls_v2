#include "../includes/ft_ls.h"

void    init_paths(t_btree *results, t_get_flags const *flags )
{
    t_sort_path sort;
    if (get_flags_is_enabled(flags, 'r'))
        ft_putendl("ofofoofo");
    if (get_flags_is_enabled(flags, 'r'))
        sort = rsort_path;
    else
        sort = sort_path;
    btree_init(results, free, sort); 
}

t_bool  begin_by_dot(char const *str)
{
    if (str && str[0] && str[0]  == '.' && str[1] != '.' && str[1])
    {
        return TRUE;
    }
    else
        return FALSE;
}

int     sort_path(char const *left, char const *right)
{
    if (begin_by_dot(left))
        left++;
    if (begin_by_dot(right))
        right++;
    return (ft_strcmp(left, right));
}

int     rsort_path(char const *left, char const *right)
{
    if (begin_by_dot(left))
        left++;
    if (begin_by_dot(right))
        right++;
    return (ft_strcmp(left, right) * -1);
}

void    list_paths(t_btree *results, t_get_flags const *flags,
        char const *dir_path)
{
    t_filter_path   filter;

    filter = (get_flags_is_enabled(flags, 'a')) ? NULL : filter_dot;
    ft_get_path(dir_path, filter, results);
}
