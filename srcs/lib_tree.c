#include "../includes/ft_ls.h"

inline static void  rbn_apply(t_btree_node *node, t_apl_func apl)
{
    apl(node->value);
}

void    btree_node_prefix(t_btree_node *node, t_apl_func apl)
{
    if (node)
    {
        rbn_apply(node, apl);
        if (node->left)
            btree_node_prefix(node->left, apl);
        if (node->right)
            btree_node_prefix(node->right, apl);
    }
}

void    btree_node_infix(t_btree_node *node, t_apl_func apl)
{
    if (node)
    {
        if (node->left)
            btree_node_infix(node->left, apl);
        rbn_apply(node, apl);
        if (node->right)
            btree_node_infix(node->right, apl);
    }
}

void    btree_node_postfix(t_btree_node *node, t_apl_func apl)
{
    if (node)
    {
        if (node->left)
            btree_node_postfix(node->left, apl);
        if (node->right)
            btree_node_postfix(node->right, apl);
        rbn_apply(node, apl);
    }
}

