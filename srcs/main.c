#include "../includes/ft_ls.h"

void	invalid_flag(t_get_flags *get_flags, t_get_flags_err err, char c)
{
	(void)get_flags;
	(void)c;
	(void)err;
}

int	main(int argc, char **argv)
{
	t_get_flags	get_flags;
	t_btree		paths;
	int		parsed_args;

	init_get_flags(&get_flags, "ar", invalid_flag);
	parsed_args = parse_args(&get_flags, argc, argv);
    init_paths(&paths, &get_flags);
    if (argc == 1)
    {
        list_paths(&paths, &get_flags, ".");
        btree_infix(&paths, ft_putendl);
        btree_clear(&paths);
    }
    else
    {
        if (parsed_args == argc)
        {
            list_paths(&paths, &get_flags, ".");
            btree_infix(&paths, ft_putendl);
            btree_clear(&paths);
        }
        else
        {
            while (parsed_args < argc)
            {
                list_paths(&paths, &get_flags, argv[parsed_args]);
                btree_infix(&paths, ft_putendl);
                btree_clear(&paths);
                parsed_args++;
            }
        }
    }
    btree_delete(&paths);
    get_flags_delete(&get_flags);
    (void)parsed_args;
	return (0);
}
