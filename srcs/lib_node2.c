#include "../includes/ft_ls.h"

t_btree_node    *btree_node_parent(t_btree_node const *node)
{
    if (node)
        return (node->parent);
    else
        return (NULL);
}

t_btree_node    *btree_node_left(t_btree_node const *node)
{
    if (node)
        return (node->left);
    else
        return (NULL);
}

t_btree_node    *btree_node_right(t_btree_node   const *node)
{
    if (node)
        return (node->right);
    else
        return (NULL);
}

void    btree_node_set_child(t_btree_node *node, t_btree_node *child,
        t_rb_child lr)
{
    if (node)
    {
        if (child)
            child->parent = node;
        if (lr == RBT_LEFT)
            node->left = child;
        else if (lr == RBT_RIGHT)
            node->right = child;
    }
}
