#include "../includes/ft_ls.h"

void    getflag_init(t_get_flags *getflag,
        char const *available_flags,
        t_get_flags_err_cb cb)
{
    const size_t    count = ft_strlen(available_flags);
    size_t  i;

    i = 0;
    getflag->fgpairs = (t_get_flags_pair *)malloc(sizeof(*getflag->fgpairs)
            * (count));
    while (i < count)
    {
        getflag->fgpairs[i].char_value = available_flags[i];
        getflag->fgpairs[i].enabled = FALSE;
        ++i;
    }
    getflag->count = count;
    getflag->error_cb = cb;
    getflag->available_flags = ft_strdup(available_flags);
}

void    get_flags_delete(t_get_flags *getflag)
{
    if (getflag->fgpairs)
    {
        free(getflag->fgpairs);
        getflag->fgpairs = NULL;
    }
    if (getflag->available_flags)
    {
        free(getflag->available_flags);
        getflag->available_flags = NULL;
    }
    getflag->count = 0;
}

t_bool  getflag_enable(t_get_flags *getflag, char c)
{
    size_t  i;

    i = 0;
    while (i < getflag->count)
    {
        if (getflag->fgpairs[i].char_value == c)
        {
            getflag->fgpairs[i].enabled = TRUE;
            return (TRUE);
        }
        ++i;
    }
    return (FALSE);
}

t_bool  getflag_is_enabled(t_get_flags const *getflag, char c)
{
    size_t  i;

    i = 0;
    while (i < getflag->count)
    {
        if (getflag->fgpairs[i].char_value == c)
            return (getflag->fgpairs[i].enabled);
        ++i;
    }
    return (FALSE);
}
char const  *getflag_available_flags(t_get_flags const *getflag)
{
    return (getflag->available_flags);
}
