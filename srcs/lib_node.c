#include "../includes/ft_ls.h"

t_btree_node    *btree_node_init(void *value)
{
    t_btree_node    *node;

    node = (t_btree_node *)malloc(sizeof(*node));
    if (node)
    {
        ft_bzero(node, sizeof(*node));
        node->value = value;
    }
    return (node);
}

void    btree_node_destroy(t_btree_node *node, t_del_func del_func)
{
    if (node)
    {
        if (node->left)
        {
            btree_node_destroy(node->left, del_func);
            node->left = NULL;
        }
        if (node->right)
        {
            btree_node_destroy(node->right, del_func);
            node->right = NULL;
        }
        if (node->value)
        {
            if (del_func)
                del_func(node->value);
            node->value = NULL;
        }
        free(node);
    }
}
