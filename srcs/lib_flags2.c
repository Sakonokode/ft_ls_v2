#include "../includes/ft_ls.h"

void	init_get_flags(t_get_flags *get_flags, char const *available_flags, t_get_flags_err_cb cb)
{
    const size_t    count = ft_strlen(available_flags);
    size_t		i;

    i = 0;
    get_flags->fgpairs = (t_get_flags_pair *)malloc(sizeof(*get_flags->fgpairs) * (count));
    while (i < count)
    {
        get_flags->fgpairs[i].char_value = available_flags[i];
        get_flags->fgpairs[i].enabled = FALSE;
        i++;
    }
    get_flags->error_cb = cb;
    get_flags->count = count;
    get_flags->available_flags = ft_strdup(available_flags);
}

static t_bool   is_flag(char *arg)
{
    return (arg[0] == '-' && ft_strlen(arg)  > 1);
}

static void     process_flag(t_get_flags *get_flags, char flag)
{
    if (get_flags_is_enabled(get_flags, flag))
    {
        if (get_flags->error_cb)
            get_flags->error_cb(get_flags, REPEATED_FLAG, flag);
    }
    else if (get_flags_enable(get_flags, flag) == FALSE)
    {
        if (get_flags->error_cb)
            get_flags->error_cb(get_flags, INVALID_FLAG, flag);
    }
}

int	parse_args(t_get_flags *get_flags, int argc, char **argv)
{
    size_t	j;
    char	*arg;
    int	    i;

    i = 1;
    while (i < argc)
    {
        arg = argv[i];
        if (ft_strequ(arg, "--"))
            return (i + 1);
        if (is_flag(arg))
        {
            j = 1;
            while (arg[j])
            {
                process_flag(get_flags, arg[j]);
                j++;
            }
        }
        else
            break;
        i++;
    }
    return (i);
}
