#include "../includes/ft_ls.h"

void    btree_delete(t_btree *tree)
{
    btree_node_destroy(tree->root, tree->del_func);
    tree->root = NULL;
}
