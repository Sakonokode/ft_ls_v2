CC = gcc
CFLAGS = -Wall -Werror -Wextra -g -I$(LIBFT_DIR)
OBJ = $(SRC:.c=.o)
NAME = ft_ls
TARGET = $(NAME)
LIBFT_DIR = libft

SRC =										\
		srcs/main.c							\
		srcs/lib_tree3.c					\
		srcs/lib_path.c						\
		srcs/lib_path2.c					\
		srcs/lib_acl.c						\
		srcs/lib_free.c						\
		srcs/lib_free2.c					\
		srcs/lib_flags.c					\
		srcs/lib_flags2.c					\
		srcs/lib_flags3.c					\
		srcs/lib_node.c						\
		srcs/lib_node2.c					\
		srcs/lib_tree.c						\
		srcs/lib_tree2.c			


$(NAME): ft $(OBJ)
	$(CC) -o $(TARGET) $(OBJ) -L$(LIBFT_DIR) -lft

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(TARGET)

re: fclean $(NAME)

all: libft $(NAME)

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

ft:
	make -C $(LIBFT_DIR)

norme:
	norminette $(SRC) *.h

