#ifndef FT_LS_H
# define FT_LS_H

# include "../libft/includes/libft.h"
# include <errno.h>
# include <stdio.h>
# include <dirent.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <pwd.h>
# include <grp.h>
# include <sys/acl.h>

# define TRUE 1
# define FALSE 0

struct s_get_flags;

typedef unsigned int    t_ui;
typedef unsigned char   t_uc;
typedef int             t_bool;                   
typedef void(*t_del_func)();
typedef int(*t_cmp_func)();
typedef void(*t_apl_func)();
typedef int(*t_filter_func)();
typedef void*(*t_clone_func)(void *);
typedef int(*t_sort_path)(char const *, char const *);
typedef t_bool(*t_filter_path)(char const *);

typedef enum	e_get_flags_err
{
	INVALID_FLAG = 0,
	REPEATED_FLAG
}	        	t_get_flags_err;

typedef void(*t_get_flags_err_cb)(struct s_get_flags *gf, t_get_flags_err type,
								char flag_char);

typedef struct	s_get_flags_pair
{
	char	char_value;
	t_bool	enabled;
}		t_get_flags_pair;

typedef struct	s_get_flags
{
	t_get_flags_pair	*fgpairs;
	char			*available_flags;
	size_t			count;
	t_get_flags_err_cb	error_cb;
}		t_get_flags;

typedef enum e_rb_child
{
        RBT_LEFT,
        RBT_RIGHT
}            t_rb_child;
 
typedef struct  s_rb_node
{
        struct s_rb_node    *parent;
        struct s_rb_node    *left;
        struct s_rb_node    *right;
        void                *value;
}               t_btree_node;

typedef struct s_btree
{
        t_btree_node    *root;
        t_del_func      del_func;
        t_cmp_func      cmp_func;
}               t_btree; 

t_bool  get_flags_enable(t_get_flags *get_flags, char c);
t_bool  get_flags_is_enabled(t_get_flags const *get_flags, char c);
void    init_get_flags(t_get_flags *get_flags, char const *available_flags, t_get_flags_err_cb cb);
int     parse_args(t_get_flags *get_flags, int argc, char **argv);
int     sort_path(char const *left, char const *right);
t_bool  begin_by_dot(char const *str);
void    init_paths(t_btree *results, t_get_flags const *flags );
int     rsort_path(char const *left, char const *right);

void  btree_init(t_btree *tree, t_del_func del_func,
        t_cmp_func cmp_func);
void    btree_node_prefix(t_btree_node *node, t_apl_func apl);
void    btree_node_infix(t_btree_node *node, t_apl_func apl);
void    btree_node_postfix(t_btree_node *node, t_apl_func apl);
void    list_paths(t_btree *results, t_get_flags const *flags,
        char const *dir_path);
t_bool  filter_dot(char const *str);
t_bool  ft_get_path(char const *dir_path, t_filter_path filter,
        t_btree *results);
void    btree_node_destroy(t_btree_node *node, t_del_func del_func);
t_btree_node    *btree_node_init(void *value);
void    btree_node_set_child(t_btree_node *node, t_btree_node *child,
        t_rb_child lr);
t_btree_node    *btree_insert(t_btree *tree, void *value);
t_btree_node    *btree_node_parent(t_btree_node   const *node);
t_btree_node    *btree_node_right(t_btree_node   const *node);
t_btree_node    *btree_node_left(t_btree_node   const *node);
void    btree_prefix(t_btree const *tree, t_apl_func apl);
void    btree_infix(t_btree const *tree, t_apl_func apl);
void    btree_postfix(t_btree const *tree, t_apl_func apl);
void    btree_delete(t_btree *tree);
void    btree_erase(t_btree *tree, t_btree_node *node);
int btree_node_is_leave(t_btree_node const *node);
int btree_node_is_root(t_btree_node const *node);
void btree_clear(t_btree *tree);
void    btree_node_destroy(t_btree_node *node, t_del_func del_func);
void    get_flags_delete(t_get_flags *get_flags);
t_bool  getflag_enable(t_get_flags *get_flags, char c);
void    getflag_init(t_get_flags *get_flags,
        char const *available_flags,
        t_get_flags_err_cb cb);
t_bool  get_flags_is_enabled(t_get_flags const *get_flags, char c);
char const  *getflag_available_flags(t_get_flags const *get_flags);
void    getflag_init(t_get_flags *getflag,
        char const *available_flags,
        t_get_flags_err_cb cb);
char const  *getflag_available_flags(t_get_flags const *getflag);



# endif
